# alpine-nextcloud
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-nextcloud)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-nextcloud)



----------------------------------------
### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-nextcloud/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-nextcloud/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [NextCloud](https://nextcloud.com/)
    - A safe home for all your data.



----------------------------------------
#### Run

```sh
docker run -d \
           -e RUN_USER_NAME=<run_user_name> \
           -p 8080:8080/tcp \
           forumi0721/alpine-nextcloud:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:8080/](http://localhost:8080/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8080/tcp           | HTTP port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /data              | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| RUN_USER_NAME      | Run user name                                    |

